<?php
	require("identifiantBD.php");

	function connexion(){
		$dsn="mysql:dbname=DBpiroird; host=servinfo-mariadb";
		try{
			$connex =new PDO($dsn,USER,PASSWD);
		}
		catch(PDOExecption $e){
			printf("Echec connexion : %s\n", $e->getMessage());
			exit();
		}
		return $connex;
	}

	function getBD($commande){
		$connex =connexion();
		$result = $connex->query($commande);
		return $result;
	}

	function ajoutJeu($titre,$annee,$editeur,$genre){
		$connex =connexion();
		$requete="insert into JEU values(:titre,:annee,:editeur,:genre)";

		$stmt=$connex->prepare($requete);
		$stmt->bindParam(':titre', $titre);
		$stmt->bindParam(':annee', $annee);
		$stmt->bindParam(':editeur', $editeur);
		$stmt->bindParam(':genre', $genre);
		$stmt->execute();

		if ($stmt){
			return true;
		}
		else{
			return false;
		}
}

	function suprJeu($titre){
		$connex =connexion();
		$requete="delete from JEU where titre like '$titre%'";
		$connex->exec($requete);
		
		if ($connex){
			return true;
		}
		else{
			return false;
		}
}
?>
