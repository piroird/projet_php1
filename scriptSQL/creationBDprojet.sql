drop table JEU;

create table JEU(
	titre VarChar(50),
	annee VarChar(4),
	editeur VarChar(20),
	genre VarChar(20),
	primary key (titre)
);
