insert into JEU(titre,annee,editeur,genre) values
  ('GTA 5','2013','Rockstar Games','Action'),
  ('Metal Gear Solid 3 : Snake Eater','2005','Kojima','Action'),
  ('Street Fighter II','1992','Capcom','Combat'),
  ('The Legend of Zelda : Breath of the Wild','2017','Nintendo','Aventure'),
  ('Deus Ex','2000','Eidos Interactive','FPS'),
  ('Super Mario 64','1997','Nintendo','Plateforme'),
  ('World of Warcraft','2005','Blizzard','MMORPG'),
  ('Final Fantasy VII','1997','Square','RPG'),
  ('The Witcher 3 : Wild Hunt','2015','Bandai Namco','RPG'),
  ('Half Life','1998','Valve','FPS'),
  ('Starcraft 2','2010','Blizzard','Stratégie');
