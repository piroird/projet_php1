<?php
	require("phpBD.php");
	require("squelettes/squeletteHaut.html");

	echo "<h1> Liste des Jeux Videos par éditeur</h1>\n";

	$lediteur=" ";
	foreach (getBD("select * from JEU order by editeur,titre") as $j) {
		if($lediteur!=$j["editeur"]){
			if($lediteur!=" "){//si pas le premier tour
				echo "</ul>\n";
			}
			echo "<ul><h2>".$j["editeur"]."</h2>";
		}
		echo "<li class='tri'><strong>".$j["titre"]."</strong>, ".$j["annee"].", ".$j["genre"]."</li>";
		$lediteur=$j["editeur"];
	}
	echo "</ul>\n";

	require("squelettes/squeletteBas.html");
?>
